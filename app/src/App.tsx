import "./App.css";
import ContentMain from "./Content";

import HomepageNavbar from "./HomepageNavbar";

function App() {
	return (
		<>
			<HomepageNavbar></HomepageNavbar>
			<ContentMain />
			<div className="wip">
				<p className="wip-text">
					This website is currently being worked on. Watch out for new content
					:)
				</p>
			</div>
		</>
		// <div className="App">
		//   <div className="aboutme">
		//     <h1>Zhongheng Liu</h1>
		//     <p>-- Web developer and GNU/Linux enthusiast.</p>
		//     <button onClick={() => { window.location.assign("/assets/resume.pdf"); }}>Learn more</button>
		//     <button onClick={() => { window.location.assign("https://gitlab.com/stvnliu"); }}>My GitLab</button>
		//   </div>
		//   <div className="links">
		//     <h1>Links</h1>
		//     <p>I host a variety of online applications on my home server. See below for available sites.</p>
		//     <h3><a href="https://git.stvnliu.me">Project Repository</a></h3>
		//     <p>A CGit frontend for the underlying Gitolite infrastructure.</p>
		//     <h3><a href="https://kellnr.stvnliu.me">Publishing site for my Rust projects</a></h3>
		//     <p>This is a site hosted with Kellnr, a service to handle uploading and downloading Rust crates from the site's API.</p>
		//     <h3><a href="https://blog.stvnliu.me">My blog</a></h3>
		//     <p>My personal blog. Content revolves around software development, mathematics, and the occasional rant.</p>
		//   </div>
		//   <div className="projects">
		//     <h1>My projects</h1>
		//     <p>These are mostly hobbyist programming projects</p>
		//     <hr></hr>
		//     <h3><a href='https://gitlab.com/proteinpedia/proteinpedia-next'>
		//       proteinpedia-next
		//     </a>
		//     </h3>
		//     <p>A content management platform for displaying and managing informatio
		//       n of proteins and other chemical</p>
		//     <hr></hr>
		//     <h3><a href="/assets/paper.pdf">How can privacy and web inter-connectivity be
		//       enshrined as core principles in self-hosted online chat applications?
		//     </a></h3>
		//     <p>An Edexcel Level 3 Artifact Extended Project Qualification (EPQ) on
		//       the topic of web chat applications, awarded an A* grade by Edexcel</p>
		//     <p>You may find the relevant GitLab repositories here:</p>
		//       <span>
		//         GitHub repositories for
		//         <a href="https://github.com/stvnliu/epq-web">web</a> and
		//         <a href="https://github.com/stvnliu/epq-api">backend API</a>
		//       </span><br />
		//       <span>
		//         GitLab repository for
		//         <a href="https://gitlab.com/stvnliu/epq-web">web</a> and
		//         <a href="https://gitlab.com/stvnliu/epq-api">backend API</a>
		//       </span>
		//   </div>
		// </div>
	);
}

export default App;
