import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
// import ContentBlock from "./Content";
function HomepageNavbar() {
	return (
		<Navbar expand="lg" className="bg-body-tertiary" data-bs-theme="dark">
			<Container>
				<Navbar.Brand href="https://stvnliu.me">My Homepage</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="me-auto">
						<Nav.Link href="https://blog.stvnliu.me">Blog</Nav.Link>
						<NavDropdown title="Links" id="basic-nav-dropdown1">
							<NavDropdown.Item href="https://gitlab.com/stvnliu">
								GitLab
							</NavDropdown.Item>
							<NavDropdown.Item href="https://www.linkedin.com/in/zhongheng-liu/">
								LinkedIn
							</NavDropdown.Item>
						</NavDropdown>
						<NavDropdown title="Projects" id="basic-nav-dropdown2">
							<NavDropdown.Item href="https://files.stvnliu.me/extended_project/EPQ-Explanation-20240424-FINAL.pdf">
								Extended Project
							</NavDropdown.Item>
							<NavDropdown.Item href="https://kellnr.stvnliu.me">
								Rust Programming Projects
							</NavDropdown.Item>
							<NavDropdown.Item href="https://github.com/Ate329/KindleRSS">
								Ate329/KindleRSS (Contrib)
							</NavDropdown.Item>
							<NavDropdown.Divider />
							<NavDropdown.Item href="https://github.com/stvnliu">
								GitHub projects
							</NavDropdown.Item>
						</NavDropdown>
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
}
export default HomepageNavbar;
