import "./Content.css";
import { Button, ButtonGroup } from "react-bootstrap";

export interface ContentProps {
  left: JSX.Element;
  right: JSX.Element;
}
/* function ContentBlock({ left, right }: ContentProps) {
  return (
    <div className="content-block-two-column-left">
      <div className="cb-left">{left}</div>
      <div className="cb-right">{right}</div>
    </div>
  );
}*/
function ContentMain() {
  return (
    <div className="base">
      <div className="first-block">
        <div className="first-block-inner">
          <h1>Steven Liu</h1>
          <p className="tagline">writing, building, and sharing code.</p>
          <p>
            <a href="https://git.stvnliu.me/stvnliu/stvnliu.gitlab.io.git/tree/?h=v2">
              → See the source code for this site.
            </a>
          </p>
          <ButtonGroup>
            <Button href="https://files.stvnliu.me/cv.pdf">
              My Resume (CV)
            </Button>
            <Button
              className="bg-body-tertiary"
              data-bs-theme="dark"
              href="https://gitlab.com/stvnliu"
            >
              My GitLab
            </Button>
            <Button
              className="bg-body-tertiary"
              data-bs-theme="dark"
              href="https://blog.stvnliu.me"
            >
              Blog
            </Button>
          </ButtonGroup>
        </div>
      </div>

      {/* <ContentBlock
				left={<p className="content-block-column-priority">Hello, </p>}
				right={<p className="content-block-column">World</p>}
			></ContentBlock> */}
    </div>
  );
}
export default ContentMain;
